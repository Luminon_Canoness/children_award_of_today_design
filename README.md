# README #

오늘의 어린이상 디자인 리소스 파일입니다

## 3an_xxhdpi ##

//공통

btn_settings : 액션바 설정 아이콘

ic_circle_plus : 플로팅 플러스 아이콘

ic_circle_ok : 플로팅 확인 아이콘


//홈 페이지

ic_tab ~ on / off : 탭바 아이콘

ic_sticker_noprofile : 프로필 없을 경우 나타나는 아이콘

stc_ref1~3 : 스티커 예제 3종

bg_card_pattern : 카드 뒤에 들어가는 패턴

ic_card_trophy : 카드 상 부분에 들어가는 트로피


//과제 페이지

ic_hwstat_sticker : 내 스티커 개수 옆의 스티커 아이콘

ic_card_sticker : 카드 얻을 스티커 갯수 옆의 스티커 아이콘

ic_card_gop : 카드 얻을 스티커 갯수 옆의 곱하기 아이콘

ic_card_earlynoti : 카드 미리알림 아이콘

ic_card_complete : 카드 다했어요 아이콘


//원해요 페이지

ic_card_sticker : 카드 필요 스티커 갯수 옆의 스티커 아이콘

ic_card_buy : 카드 '스티커로 살래요' 버튼 아이콘


// 통계 페이지

ic_tonggye_trophy : 통계페이지 최근 받은상 탭 트로피 아이콘


//로그인 폼 페이지

bg_splash : 로그인 스플래시 배경화면
title_splash : 로그인 스플래시 로고


//튜토리얼 페이지

bg_splash : 일부 튜토리얼 배경화면
tt_title ~ : 각 튜토리얼 페이지의 제목
tt_smartphone : 스마트폰 일러스트


## COLORS ##

//공통

메인 컬러 : #F491B3 /*핑크색*/

서브 컬러 : #9575CD /*보라색*/

상단바 컬러 : #EF6392

백그라운드 : #EFEFEF


//스티커 페이지

프로파일 없을때 나타나는 색 / 보조 텍스트 : #CFB9F5

스티커판 스티커 없을때 : #EEEEEE


//과제 페이지

내 스티커 갯수 글귀 / ~개 : #F9E7ED

~을 받을때까지 글귀 / ~개 필요 : #E8E3F1

과제 카드 제목부분 배경 : #FF9E80

카드 디바이더 색 : #EEEEEE

날짜 : #FFE9E3


//과제 추가 페이지 & 원하는 것 추가 페이지

글 작성 전 비활성 글씨 색 : #BDBDBD

디바이더 색 : #EEEEEE

활성 글 색(회색) : #898989


//원해요 페이지

원해요 카드 제목부분 배경 : #4DB6AC

날짜 : #D2EDEA

카드 디바이더 색 : #EEEEEE